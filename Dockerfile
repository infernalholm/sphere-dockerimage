FROM i386/debian:stable-slim
LABEL Description="This image is used to fetch and install SphereServer 0.56b, \
the server will be installed in /sphere and expects the script files to be in \
the same folder" Version=1.0.0

WORKDIR /sphere

RUN apt-get update --fix-missing \
  && apt-get dist-upgrade -y \
  && apt-get install -y \
    curl \
    mysql-common \
  && apt-get clean;

# libmysql Dependency
RUN curl -L -o libmysqlclient.deb https://launchpadlibrarian.net/94563300/libmysqlclient16_5.1.58-1ubuntu5_i386.deb \
  && dpkg -i libmysqlclient.deb \
  && rm -rf libmysqlclient.deb

# Install Sphere 
RUN curl -L -o SphereServer-0.56b-201306161304-Linux.tgz https://forum.spherecommunity.net/sshare.php?downproj=48 \
  && tar -xzf SphereServer-0.56b-201306161304-Linux.tgz \
  && rm -rf SphereServer-0.56b-201306161304-Linux.tgz

# Clean up
RUN apt-get remove --purge -y curl;
RUN apt-get autoremove -y
